<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/style.css-ver=5.0.css'); ?>">                
<style>.article-content{font-size: 16px;line-height:27px;}.article-content p{margin:20px 0;}@media (max-width:640px){.article-content{font-size:16px}}body{margin-top: 52px}.navbar-wrap{position:fixed}@media (max-width: 979px){body{margin-top: 0}.navbar-wrap{position:relative}}</style>
<meta name='robots' content='noindex,follow' />
<script type="text/javascript">                                                                                                                                                                      
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"<?php $this->options->themeUrl('/css/editor-style.css'); ?>"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<script>
window._deel = {
    name: '<?php $this->options->title() ?>',
    url: '<?php $this->options->siteUrl(); ?>',
    rss: '',
    ajaxpager: '',
    maillist: '',
    maillistCode: '',
    commenton: 1,
    roll: [1,],
    tougaoContentmin: 200,
    tougaoContentmax: 5000,
    appkey: {
    	tqq: '1713281933',
    	tsina: '1713281933',
    	t163: '',
    	tsohu: ''
    }
}
</script>

<meta charset="<?php $this->options->charset(); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php $this->archiveTitle(array(
            'category'  =>  _t('分类 %s 下的文章'),
            'search'    =>  _t('包含关键字 %s 的文章'),
            'tag'       =>  _t('标签 %s 下的文章'),
            'author'    =>  _t('%s 发布的文章')
        ), '', ' - '); ?><?php $this->options->title(); ?></title>
    <?php $this->header(); ?>
</head>
<body class="home blog">
<div class="navbar-wrap">
<div class="navbar">
<h1 class="logo"><a href="<?php $this->options->siteUrl(); ?>" title="<?php $this->options->title() ?><"><?php $this->options->title() ?></a></h1>
<ul class="nav active" style="display">
<li><a href="<?php $this->options->siteUrl(); ?>">首页</a></li>
  <?php $this->widget('Widget_Metas_Category_List')
               ->parse('<li id="menu-item-17" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-17"><a href="{permalink}">{name}</a></li>'); ?>
</ul>
<div class="screen-mini">
<button data-type="screen-nav" class="btn btn-inverse screen-nav">
<i class="icon-tasks icon-white"></i>
</button>
</div>
<div class="menu pull-right">
<form method="get" class="dropdown search-form" action="<?php $this->options->siteUrl(); ?>" >
<input class="search-input" name="s" type="text" placeholder="输入关键字搜索" x-webkit-speech=""><input class="btn btn-success search-submit" type="submit" value="搜索">
<ul class="dropdown-menu search-suggest"></ul>
</form>
<div class="btn-group pull-left">
<button class="btn btn-primary" data-toggle="modal" data-target="">订阅</button>
<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">关注 <i class="caret"></i></button>
<ul class="dropdown-menu pull-right">
</div>
</div>
</div>
</div>
<header class="header">
<div class="speedbar">
<div class="toptip"><strong class="text-success">联系方式：</strong>
<a href="tencent://message/?uin=1713281933&Site=&Menu=yes" target="_blank">QQ：1713281933 E-mail：1713281933@qq.com</a>
</div>
</div>
</header>