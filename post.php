<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<section class="container">
	<div class="content-wrap">
	<div class="content">
		<div class="breadcrumbs">你的位置：<?php if($this->is('index')):?><!-- 页面首页时 -->
	<a href="<?php $this->options->siteUrl(); ?>" title="<?php $this->options->title(); ?>">首页</a> &gt;	
	<?php elseif ($this->is('post')): ?><!-- 页面为文章单页时 -->
		<a href="<?php $this->options->siteUrl(); ?>" title="<?php $this->options->title(); ?>">首页</a> &gt; <?php $this->category(); ?> &gt; <?php $this->title(); ?>
	<?php else: ?><!-- 页面为其他页时 -->
		<a href="<?php $this->options->siteUrl(); ?>" title="<?php $this->options->title(); ?>">首页</a> &gt; <?php $this->archiveTitle(' &raquo; ','',''); ?>
	<?php endif; ?></div>

				<header class="article-header">
			<h1 class="article-title"><a href="<?php $this->permalink() ?>"><?php $this->title() ?></a></h1>
			<div class="meta">
				<span class="muted"><i class="icon-user icon12"></i> 作者：<?php $this->author(); ?></span>
<span class="muted"><i class="icon-list-alt icon12"></i> <?php $this->category(' '); ?></span>
				<time class="muted" datetime="<?php $this->date('c'); ?>" title="<?php $this->date('y-m-d'); ?>"><i class="ico icon-time icon12"></i> 时间：<?php $this->date('y-m-d'); ?></time>
				
										</div>  
		</header>

 <article class="article-content">
                <?php $this->content(); ?>
                <footer class="article-footer">
                    <div class="article-tags">继续浏览有关 <?php $this->tags(' ', true, 'none'); ?> 的文章</div> 
<div class="article-tags"></div>
</footer></article>
<nav class="article-nav">
<span class="article-nav-prev"><?php $this->thePrev("上一篇：%s"); ?></span>
<span class="article-nav-next"><?php $this->theNext("下一篇：%s"); ?></span>
</nav>
<div class="relates">
<h3>与本文相关的文章</h3>
<ul>
<?php $this->related(10)->to($relatedPosts); ?>
    <ul>
    <?php while ($relatedPosts->next()): ?>
    <li><a href="<?php $relatedPosts->permalink(); ?>" title="<?php $relatedPosts->title(); ?>"><?php $relatedPosts->title(); ?></a></li>
    <?php endwhile; ?>
</ul>
	</div>



         
            <?php $this->need('comments.php'); ?>

        </div>
    </div>
<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
</section>