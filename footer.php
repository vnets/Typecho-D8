<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<footer class="footer">
    <div class="footer-inner">
        <div class="copyright pull-left">
            版权所有，保留一切权利！ &copy; 2018 <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title() ?></a> <a href="//www.typecho.org"> Powered by Typecho</a>. <a href="tencent://message/?uin=1713281933&Site=&Menu=yes">Theme. 59,!</a>
      </div>
        <div class="trackcode pull-right">
            <div style="display:none">
  
    
</div>        </div>
    </div>
</footer>


<script type='text/javascript' src='<?php $this->options->themeUrl('js/jquery.js'); ?>'></script>
<script type='text/javascript' src='<?php $this->options->themeUrl('js/wp-embed.min.js'); ?>'></script>
<?php $this->footer(); ?>

