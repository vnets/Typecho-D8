<?php
/**
 * 
 * Typecho-D8主题基于Typecho程序，响应式布局支持电脑、平板和手机的完美展示
 * @package Typecho-D8
 * @author 59,!
 * @version 1.0.1
 * @link tencent://message/?uin=1713281933&Site=&Menu=yes
 */
 ?>

<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<section class="container">

	<div class="content-wrap">
	<div class="content">
	<div class="sticky">
	
</div>
<section class="container">
	<h2 class="title"> 最新发布</h2>
<?php while($this->next()): ?>

<article class="excerpt excerpt-titletype">
		<div class="focus">
		<a href="<?php $this->permalink() ?>" class="thumbnail">

 <?php if ($this->options->RandomPicChoice !=='0' && array_key_exists('thumb',unserialize($this->___fields()))): ?>
	    <img src="<?php echo $this->fields->thumb; ?>"/>
	    <?php else: ?>
          <?php $thumb = showThumbnail($this); if(!empty($thumb)): ?>
	 <img src="<?php echo $thumb ?>"/>
	   <?php endif; ?>
        <?php endif; ?>
</a></div>
		<header>
				<h2><a href="<?php $this->permalink() ?>" title="<?php $this->title() ?>"><?php $this->title() ?></a></h2>
	</header>
	<p>
				<span class="muted"><i class="icon-user icon12"></i> 作者：<?php $this->author(); ?></span>
<span class="muted"><i class="icon-list-alt icon12"></i> <?php $this->category(' '); ?></span>
				<time class="muted" datetime="<?php $this->date('c'); ?>" title="<?php $this->date('y-m-d'); ?>"><i class="ico icon-time icon12"></i> 时间：<?php $this->date('y-m-d'); ?></time>
				</p>	<p class="note">
		<?php $this->excerpt(50, '...'); ?>  </p>
</article>
<?php endwhile; ?>
</article>
<div class="pagination">
<ul>
<li class="prev-page"></li>
<a href='<?php $this->options->siteUrl(); ?>'>首页</a>
                    <?php $this->pageLink('<x aria-label="Previous" class="newer-posts"><i class="fa fa-angle-left"></i></x>'); ?>
    <span class="page-number">第 <?php if($this->_currentPage>1) echo $this->_currentPage;  else echo 1;?> 页 / 共 <?php echo ceil($this->getTotal() / $this->parameter->pageSize); ?> 页</span>
        <?php $this->pageLink('<x aria-label="Next" class="older-posts">下一页<i class="fa fa-angle-right"></i></x>','next'); ?></ul></div>	</div>
</div>
<?php $this->need('sidebar.php'); ?>

<?php $this->need('footer.php'); ?>