<aside class="sidebar">	
<div class="widget widget_recent_entries"><h3 class="widget_tit">热门推荐</h3>
<ul>
<?php TePostViews_Plugin::outputHotPosts() ?>


</ul>

</div>		<div class="widget widget_recent_entries">		<h3 class="widget_tit">随机文章</h3>		
	<ul>
<?php RandomArticleList::parse(); ?>

</ul>
		</div>
  <div class="widget d_tag">
<div class="widget d_tag">
<h3 class="widget_tit">标签云</h3>
<div class="d_tags">
<?php $this->widget('Widget_Metas_Tag_Cloud', 'sort=mid&ignoreZeroCount=1&desc=0&limit=100')->to($tags); ?>
<?php if($tags->have()): ?>
<?php while ($tags->next()): ?>
<a rel="tag" href="<?php $tags->permalink(); ?>"><?php $tags->name(); ?></a>
<?php endwhile; ?>
<?php else: ?>
    <a href="<?php $this->options->siteUrl(); ?>"><?php _e('没有任何标签'); ?></a>
<?php endif; ?>
</div>
</div>
<div class="widget widget_recent_entries">		
<h3 class="widget_tit">站点统计</h3>		
<ul>
<?php Typecho_Widget::widget('Widget_Stat')->to($stat); ?>
<li>文章总数：<?php $stat->publishedPostsNum() ?>篇</li>
<li>分类总数：<?php $stat->categoriesNum() ?>个</li>
<li>评论总数：<?php $stat->publishedCommentsNum() ?>条</li>
<li>运行时长：<?php getBuildTime(); ?></li>
</ul>
		</div>
	
<div class="widget widget_recent_entries">		
<h3 class="widget_tit">友情链接</h3>		
<ul>
<li><a href="tencent://message/?uin=1713281933&Site=&Menu=yes">By 59,!</a></li>
</ul>
		</div>

</aside></section>
