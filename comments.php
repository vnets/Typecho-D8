<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<div id="respond" class="no_webshot">
    <?php $this->comments()->to($comments); ?>
    <?php if ($comments->have()): ?>

	<div class="comt-title">
			<div class="comt-avatar pull-left">
				<img src="https://cdn.v2ex.com/gravatar" class="avatar avatar-56" height="56" width="56">			</div>
			<div class="comt-author pull-left">
			网友最新评论（<?php $this->commentsNum(_t('0'), _t('1'), _t(' %d ')); ?>）</div>
		</div>
	
	 
    
  <?php $this->comments()->to($comments); ?>
        <?php while($comments->next()): ?>
	  <div id="postcomments"> <h3 id="comments"></h3>	
<ol class="commentlist">
	 <li id="comment-<?php $comments->theId(); ?>" class="comment comment-parent depth-1 thread-odd"> 
	 <div class="c-avatar">
	 <img class="avatar" data-original="https://cdn.v2ex.com/gravatar" width="36" height="36" /></div> 
	 <div class="c-main" id="div-comment-<?php $comments->theId(); ?>"> 
	 <p><?php $comments->content(); ?></p> 
	 <div class="c-meta"> 
	 <span class="c-author"><?php $comments->author(); ?></span><?php $comments->date('F jS, Y'); ?>
	</div> </div> </li>
	

</div>
<?php endwhile; ?>
<div class="comt-title"><div class="comt-author pull-left"><?php $comments->pageNav('上一页', '下一页'); ?> </div></div>
    
    <?php endif; ?>

    
    <?php if($this->allow('comment')): ?>
	<div id="<?php $this->respondId(); ?>" class="respond">
		<form action="<?php $this->commentUrl() ?>" id="comment-form" method="post">
		
		<div class="comt-title">
			<div class="comt-avatar pull-left">
				<img src="https://cdn.v2ex.com/gravatar/avatar" class="avatar avatar-56" height="56" width="56">			</div>
			<div class="comt-author pull-left">
			发表我的评论			</div>
		</div>
		
		<div class="comt">
			<div class="comt-box">
				<textarea placeholder="写点什么..." class="input-block-level comt-area" name="text" id="textarea" cols="100%" rows="3" tabindex="1"><?php $this->remember('text'); ?></textarea>
				<div class="comt-ctrl">
					<button class="btn btn-primary pull-right" type="submit" tabindex="5"><i class="icon-ok-circle icon-white icon12"></i> 提交评论</button>
				</div>
				<?php $comments->cancelReply('<i class="fa fa-times fa-lg" aria-hidden="true" title="取消回复"></i>'); ?>
			</div>
            <?php if($this->user->hasLogin()): ?>
									<div class="col order-first">
										<div style="line-height:30px;">
											<?php _e('当前用户: '); ?><a href="<?php $this->options->profileUrl(); ?>"><?php $this->user->screenName(); ?></a>
											<a href="<?php $this->options->logoutUrl(); ?>" title="Logout"><?php _e('退出'); ?></a>
										</div>                                    
									</div>
								<?php else: ?>
								<div class="comt-comterinfo" id="comment-author-info" style="display: block;">
						<h4>Hi，您需要填写昵称和邮箱！</h4>
						<ul>
							<li class="form-inline"><label class="hide" for="author">昵称</label><input type="text" name="author" id="author" placeholder="<?php _e('昵称'); ?>" value="<?php $this->remember('author'); ?>" aria-label="Username" aria-describedby="ico-name" required /><span class="help-inline">昵称 (必填)</span></li>
							<li class="form-inline"><label class="hide" for="email">邮箱</label><input type="email" name="mail" id="mail" placeholder="<?php _e('邮箱'); ?>" value="<?php $this->remember('mail'); ?>" aria-label="Email" aria-describedby="ico-email" <?php if ($this->options->commentsRequireMail): ?> required <?php endif; ?> /><span class="help-inline">邮箱 (必填)</span></li>
							<li class="form-inline"><label class="hide" for="url">网址</label>
							<input type="url" name="url" id="url" placeholder="<?php _e('网站'); ?>" value="<?php $this->remember('url'); ?>" aria-label="Email" aria-describedby="ico-url" <?php if ($this->options->commentsRequireURL): ?> required <?php endif; ?> /><span class="help-inline">网址</span></li>
						</ul>
					</div>
								<?php endif; ?>
									</div>

		
	</form>
	</div>
    <?php else: ?>
    <h3><?php _e('评论已关闭'); ?></h3>
    <?php endif; ?>
</div>